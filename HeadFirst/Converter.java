import java.io.*;
import java.util.*;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.lang.*;

public class Converter {
  public String ConvertToHex(String Dec) {
  //make sure each string can be slpit into
  String EqualString = ConvertToBinary(Dec);

  //make sure there are no whitespaces in String
  EqualString = EqualString.replaceAll("\\s", "");
  int BaseNumber = Integer.parseInt(Dec);
  for (int i = 0; i < Math.abs(EqualString.length()%4); i++) {
    EqualString = "0" + EqualString;
  }

  System.out.print(EqualString + "Equal string after zeros added");
    //create a hashmap
  Map<String,String> HexCharacters =  new HashMap<>();
  HexCharacters.put("0001","1");
  HexCharacters.put("0010","2");
  HexCharacters.put("0011","3");
  HexCharacters.put("0100","4");
  HexCharacters.put("0101","5");
  HexCharacters.put("0110","6");
  HexCharacters.put("0111","7");
  HexCharacters.put("1000","8");
  HexCharacters.put("1001","9");
  HexCharacters.put("1010","A");
  HexCharacters.put("1011","B");
  HexCharacters.put("1100","C");
  HexCharacters.put("1101","D");
  HexCharacters.put("1110","E");
  HexCharacters.put("1111","F");
  //create empty String
  //for loop to iterate through string
  //creates an array to loop through

  // Creating array of string length
  char[] EqualStringArray = new char[EqualString.length()];

  // Copy character by character into array
  for (int i = 0; i < EqualString.length(); i++) {
     EqualStringArray[i] = EqualString.charAt(i);
  }
 //   System.out.print(EqualString + " ");
 // for (char c : EqualStringArray) {
 //   System.out.println(c);
 //   }
 // String[] EqualStringArray = new String[]{EqualString};
  //System.out.println(EqualStringArray);
  String ReturnString = "";
       for (int i = 0; i+4 <= EqualStringArray.length; i += 4) {
           String FourSection = Character.toString(EqualStringArray[i])+ Character.toString(EqualStringArray[i+1])+ Character.toString(EqualStringArray[i+2])+ Character.toString(EqualStringArray[i+3]);
           System.out.print(FourSection + " Foursection:" + Integer.toString(i) );
           if (HexCharacters.get(FourSection) == null) {
             ReturnString =  ReturnString + "0";
           } else {
           ReturnString = ReturnString + HexCharacters.get(FourSection);
         }
       }
      System.out.print(" "+ ReturnString);
      return ReturnString;

  //put in the Binary: Hex

  }
  public String ConvertToBinary(String Dec)  {
      // array to store binary number - 32 bits
      int[] binaryNum = new int[32];

      // counter for binary array
      int n = Integer.parseInt(Dec);
      int i = 0;
      //while the number is greater than 0 (int so it will round to 0 once divided into 0.5)
      while (n > 0) {
          // storing remainder in binary array
          binaryNum[i] = n % 2; //stores remainder (1 or 0 as integer)
          n = n / 2; // set n to its qoutient, taking into account the remiander due to rounding of int (e.g. 6/3 = 2, 7/2 = 3 (1 was already stored in array))
          i++; //increases the counter of zeros in the array
      }
      //create empty
      String EntireNumber = "";
      // printing binary array in reverse order
      for (int j = i - 1; j >= 0; j--) {
         // System.out.print(binaryNum[j]);
          EntireNumber = EntireNumber + Integer.toString(binaryNum[j]);
          //starts j at the counter position, but subtracts 1 because arrays start at zero and the condition is zero
        }
        return EntireNumber;
  }
  public String ConvertToOcto(String Dec) {
    // TURNS OUT; WE CAN JUST USE THE INTEGER OBJECT FUNCTIONS
    //
      return Integer.toOctalString(Integer.parseInt(Dec));
    }
  public String ConvertToInt(String Dec, int Base){
      int DecimalNumber = Integer.parseInt(Dec);
      return Integer.toString(Integer.parseInt(Dec, Base));
  }
}
