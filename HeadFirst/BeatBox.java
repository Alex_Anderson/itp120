
// chapter 13

import java.awt.*;
import javax.swing.*;
import javax.sound.midi.*;
import java.util.*;
import java.awt.event.*;
import java.io.*;

public class BeatBox {

    JPanel mainPanel;
    ArrayList<JCheckBox> checkboxList;
    Sequencer sequencer;
    Sequence sequence;
    Track track;
    JFrame theFrame;

    String[] instrumentNames = {"Bass Drum", "Closed Hi-Hat", 
       "Open Hi-Hat","Acoustic Snare", "Crash Cymbal", "Hand Clap", 
       "High Tom", "Hi Bongo", "Maracas", "Whistle", "Low Conga", 
       "Cowbell", "Vibraslap", "Low-mid Tom", "High Agogo", 
       "Open Hi Conga"};
    int[] instruments = {35,42,46,38,49,39,50,60,70,72,64,56,58,47,67,63};
    

    public static void main (String[] args) {
        new BeatBox().buildGUI();
    }
  
    public void buildGUI() {
        theFrame = new JFrame("Cyber BeatBox");
        theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BorderLayout layout = new BorderLayout();
        JPanel background = new JPanel(layout);
        background.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        checkboxList = new ArrayList<JCheckBox>();
        Box buttonBox = new Box(BoxLayout.Y_AXIS);

        JButton start = new JButton("Start");
        start.addActionListener(new MyStartListener());
        buttonBox.add(start);         
          
        JButton stop = new JButton("Stop");
        stop.addActionListener(new MyStopListener());
        buttonBox.add(stop);

        JButton upTempo = new JButton("Tempo Up");
        upTempo.addActionListener(new MyUpTempoListener());
        buttonBox.add(upTempo);

        JButton downTempo = new JButton("Tempo Down");
        downTempo.addActionListener(new MyDownTempoListener());
        buttonBox.add(downTempo);

	JButton clearAll = new JButton("Clear");
	clearAll.addActionListener(new clearAllListener());
	buttonBox.add(clearAll);

	JButton inverseAll = new JButton("Inverse");
	inverseAll.addActionListener(new inverseAllListener());
	buttonBox.add(inverseAll);
	//needs fixing
	JTextField rowFill = new JTextField("Fill a Row");
	rowFill.addActionListener(new rowFillListener());
	buttonBox.add(rowFill);

	JButton slideRight = new JButton("slideRight");
	slideRight.addActionListener(new slideRightListener());
	buttonBox.add(slideRight);
	
	JButton slideLeft = new JButton("slideLeft");
	slideLeft.addActionListener(new slideLeftListener());
	buttonBox.add(slideLeft);

    JButton savePattern = new JButton("Save Pattern Snapshot"); //snapshot captures it
    savePattern.addActionListener(new MySendListener());
	buttonBox.add(savePattern);

    JButton readPattern = new JButton("Revert Pattern");
    readPattern.addActionListener(new MyReadInListener());
	buttonBox.add(readPattern);

        Box nameBox = new Box(BoxLayout.Y_AXIS);
        for (int i = 0; i < 16; i++) {
           nameBox.add(new Label(instrumentNames[i]));
        }
        
        background.add(BorderLayout.EAST, buttonBox);
        background.add(BorderLayout.WEST, nameBox);

        theFrame.getContentPane().add(background);
          
        GridLayout grid = new GridLayout(16,16);
        grid.setVgap(1);
        grid.setHgap(2);
        mainPanel = new JPanel(grid);
        background.add(BorderLayout.CENTER, mainPanel);

        for (int i = 0; i < 256; i++) {                    
            JCheckBox c = new JCheckBox();
            c.setSelected(false);
            checkboxList.add(c);
            mainPanel.add(c);            
        } // end loop

        setUpMidi();

        theFrame.setBounds(50,50,300,300);
        theFrame.pack();
        theFrame.setVisible(true);
    } // close method


    public void setUpMidi() {
      try {
        sequencer = MidiSystem.getSequencer();
        sequencer.open();
        sequence = new Sequence(Sequence.PPQ,4);
        track = sequence.createTrack();
        sequencer.setTempoInBPM(120);
        
      } catch(Exception e) {e.printStackTrace();}
    } // close method

    public void buildTrackAndStart() {
      int[] trackList = null;
    
      sequence.deleteTrack(track);
      track = sequence.createTrack();

        for (int i = 0; i < 16; i++) {
          trackList = new int[16];
 
          int key = instruments[i];   

          for (int j = 0; j < 16; j++ ) {         
              JCheckBox jc = (JCheckBox) checkboxList.get(j + (16*i));
              if ( jc.isSelected()) {
                 trackList[j] = key;
              } else {
                 trackList[j] = 0;
              }                    
           } // close inner loop
         
           makeTracks(trackList);
           track.add(makeEvent(176,1,127,0,16));  
       } // close outer

       track.add(makeEvent(192,9,1,0,15));      
       try {
           sequencer.setSequence(sequence); 
	     sequencer.setLoopCount(sequencer.LOOP_CONTINUOUSLY);                   
           sequencer.start();
           sequencer.setTempoInBPM(120);
       } catch(Exception e) {e.printStackTrace();}
    } // close buildTrackAndStart method
            
           
    public class MyStartListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            buildTrackAndStart();
        }
    } // close inner class
    
    public class clearAllListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
	    int x = 2;
	    for (JCheckBox cb: checkboxList){
	    	x = 3;
		cb.setSelected(false);
	    }	    
	}
    } // close inner class
    public class MyStopListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            sequencer.stop();
        }
    } // close inner class

    public class MyUpTempoListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
	      float tempoFactor = sequencer.getTempoFactor(); 
            sequencer.setTempoFactor((float)(tempoFactor * 1.03));
        }
     } // close inner class

     public class MyDownTempoListener implements ActionListener {
         public void actionPerformed(ActionEvent a) {
	      float tempoFactor = sequencer.getTempoFactor();
            sequencer.setTempoFactor((float)(tempoFactor * .97));
        }
    } // close inner class

    public class inverseAllListener implements ActionListener {
	public void actionPerformed(ActionEvent a) {
	for (JCheckBox cb: checkboxList){
	    if(cb.isSelected() == true){
		cb.setSelected(false);
	    } else {
		cb.setSelected(true);
	    }
	}
    }
    }
    public class slideRightListener implements ActionListener {
	public void actionPerformed(ActionEvent a){
	    for (int i = 255; i>0; i--){
		JCheckBox cb = checkboxList.get(i);
            	if(cb.isSelected() == true) {
		    cb.setSelected(false);
		    cb = checkboxList.get(i+1);
		    cb.setSelected(true);
		    i++;
		}
	    }
	}
    }
    public class slideLeftListener implements ActionListener {
        public void actionPerformed(ActionEvent a){
            for (int i = 0; i<255; i++){
                JCheckBox cb = checkboxList.get(i);
                if(cb.isSelected() == true) {
                    cb.setSelected(false);
                    cb = checkboxList.get(i-1);
                    cb.setSelected(true);
                }
            }
        }
    }
    public class MySendListener implements ActionListener { //listener
        public void actionPerformed(ActionEvent a) { 
        boolean[] checkboxState = new boolean[256]; //creates an array of 256 booleans
        for (int i = 0; i < 256; i++) { //iterates through i 256 times, with i increasing each time - effectively accounting for each checkbox/boolean value
        JCheckBox check = (JCheckBox) checkboxList.get(i); //check points to the box's I position in the checkboxlist
        if (check.isSelected()) { //checks if the jcheckbox is selected
        checkboxState[i] = true; //saves its state in the boolean (true = selected)
        }
        }
        try { //creates a file and output stream, and then writes the boolean data to that stream
        FileOutputStream fileStream = new FileOutputStream(new File("Checkbox.ser"));
        ObjectOutputStream os = new ObjectOutputStream(fileStream);
        os.writeObject(checkboxState);
        } catch(Exception ex) {
        ex.printStackTrace();
        }
        } // close method
    } // close inner class
    public class MyReadInListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
        boolean[] checkboxState = null; //creates empty pointer
        try {
            //opens the file stream 
        FileInputStream fileIn = new FileInputStream(new File("Checkbox.ser"));
        ObjectInputStream is = new ObjectInputStream(fileIn);
        //creates an array using logical operators, reads the object's boolean statepoint
        checkboxState = (boolean[]) is.readObject();
       
        } catch(Exception ex) {ex.printStackTrace();}
        for (int i = 0; i < 256; i++) {
            //iterates 256 times again, see upper class for better explanation
        JCheckBox check = (JCheckBox) checkboxList.get(i);
        if (checkboxState[i]) {
            //if the checkbox boolean array saved said it was true, it sets check to true
        check.setSelected(true);
        } else {
        check.setSelected(false);
        }
        }
        sequencer.stop();
        buildTrackAndStart();
        //builds track
        } // close method
    } // close inner class

//  fill row, commented out until fixed
    public class rowFillListener implements ActionListener {
	public void actionPerformed(ActionEvent a) {
//		String rowInput = rowFill.getText();
//        	int rowToBeFilled = Integer.parseInt(rowInput);	
//		for (int i = (rowToBeFilled-1)*16; i <= (rowToBeFilled-1)*16+15; i++) {
//			JCheckBox Jc = checkboxList.get(i);
//			Jc.setSelected(true);
//		}
	}
  } // close inner class
    public void makeTracks(int[] list) {        
       
       for (int i = 0; i < 16; i++) {
          int key = list[i];

          if (key != 0) {
             track.add(makeEvent(144,9,key, 100, i));
             track.add(makeEvent(128,9,key, 100, i+1));
          }
       }
    }
        
    public  MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);

        } catch(Exception e) {e.printStackTrace(); }
        return event;
    }

}

