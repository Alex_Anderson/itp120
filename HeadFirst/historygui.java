import javax.swing.*;
import java.awt.event.*;
import java.util.Random;


public class historygui implements ActionListener {
 JButton button;
 public static void main (String[] args) {
 historygui gui = new historygui();
 gui.go();
 }
 public void go() {
 JFrame frame = new JFrame();
 button = new JButton("click me for your grade");
 button.addActionListener(this);
 frame.getContentPane().add(button);
 frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 frame.setSize(300,300);
 frame.setVisible(true);
 }
 public void actionPerformed(ActionEvent event) {
   Random rand = new Random();
   int randomEvent = rand.nextInt(4);
   int randomGrade = rand.nextInt(80);
   if (randomEvent == 1){
     randomGrade = randomGrade - 30;
      button.setText("Your grade is " + randomGrade + ", -30 for footnote errors");
   }
   if (randomEvent == 2){
     randomGrade = randomGrade - 20;
      button.setText("You turned it in late. 20 points off" + " " + randomGrade);
   }
   if (randomEvent == 3){
     randomGrade = randomGrade;
      button.setText("Great Start!" + randomGrade);
   }
   if (randomEvent == 4){
     randomGrade = randomGrade - 100;
      button.setText("Qoutes cannot be used as sentences." + "" + randomGrade);

   }
 }
}
