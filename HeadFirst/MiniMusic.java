import javax.sound.midi.*;
import java.util.concurrent.TimeUnit;

public class MiniMusic {

    public static void main(String[] args) {
        MiniMusic mini = new MiniMusic();
        //NOT ENOUGH INFORMATION TO PLAY SOUND
        if (args.length < 2) {
            System.out.println("Don't forget the instrument and note args");
            //IF TWO ARGUMENTS (NOTE AND INSTRUMENT SPECIFIED)
        } else if (args.length == 2)  {
            int instrument = Integer.parseInt(args[0]);
            int note = Integer.parseInt(args[1]);
            mini.play(instrument,note);
            //IF THREE ARGUMENTS (REPRITION SPECIFIED)
        } else if (args.length == 3) {
          int times = Integer.parseInt(args[0]);
          int instrument = Integer.parseInt(args[1]);
          int note = Integer.parseInt(args[2]);
          MusicCommands.SpecificNotes(times, instrument, note);
          //IF 4 ARGUMENTS (SPEED OF REPTITION SPECIFIED)
        } else if (args.length == 4){
          int speed = Integer.parseInt(args[0]);
          int times = Integer.parseInt(args[1]);
          int instrument = Integer.parseInt(args[2]);
          int note = Integer.parseInt(args[3]);
          MusicCommands.SpecificNotes(times, instrument, note);

        }
    }
    //STATIC CLASS TO HOLD MUSIC COMMANDS
    public static class MusicCommands {
      //methods for playing notes a specific number of times - MISNAMED CLASS
      public static void SpecificNotes(int times, int instrument, int note) {
        for (int i = times; i > 0; i--){
          MiniMusic mini = new MiniMusic();
          mini.play(instrument,note);
          try {
            TimeUnit.SECONDS.sleep(2);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
        }
      }
      //overloading for playing notes in a faster reptition
      public static void SpecificNotes(int speed, int times, int instrument, int note){
        for (int i = times; i > 0; i--){
          MiniMusic mini = new MiniMusic();
          mini.play(instrument,note);
          try {
            TimeUnit.SECONDS.sleep(2/speed);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
        }
      }



    //play method; general black box
    }
    public void play(int instrument, int note) {
        try {
            Sequencer player = MidiSystem.getSequencer();
            player.open();
            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();
            MidiEvent event = null;

            ShortMessage first = new ShortMessage();
            first.setMessage(192, 1, instrument, 0);
            MidiEvent changeInstrument = new MidiEvent(first, 1);
            track.add(changeInstrument);

            ShortMessage a = new ShortMessage();
            a.setMessage(144, 1, note, 100);
            MidiEvent noteOn = new MidiEvent(a, 1);
            track.add(noteOn);

            ShortMessage b = new ShortMessage();
            b.setMessage(128, 1, note, 100);
            MidiEvent noteOff = new MidiEvent(b, 1);
            track.add(noteOff);

            player.setSequence(seq);
            player.start();
            //suggestion to add Thread to make java end after note played (?) Did not work, indefinite
            Thread.sleep(5);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
